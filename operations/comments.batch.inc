<?php
// $Id: comments.batch.inc,v 1.1.2.3 2010/12/19 12:02:14 jonhattan Exp $

require_once 'base.batch.inc';

/**
 * Batch operation. Import dadaIMC comments.
 */
class dadaImportComments extends BaseBatchOperation implements iBatchOperation {
  public $token = 'comments';

  function getTotal() {
    $query = 'SELECT COUNT(objectid) FROM comments WHERE deleted = 0';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }

  function process($current, $total) {
    $query = "SELECT * FROM comments WHERE deleted = 0 ORDER BY objectid LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {

      $nid = $this->getArticleNid($row->rootid, $this->class_to_identifier($row->root_class));
      if ($nid) {
      $pid = 0;
      $comment = array(
        'nid' => $nid,
        'subject' => $this->encode($row->heading),
        'comment' => $this->encode($row->summary) ."\n". $this->encode($row->body),
        'format' => 1,
        'pid' => $pid,
        'uid' => 0,
        'name' => $this->encode($row->author),
      );
      if ($row->related_url1 || $row->related_url2) {
       $comment['comment'] .= '<br /><p><strong>Related</strong></p>';
      }
      if ($row->related_url1) {
        $comment['comment'] .= '<p><a href="'. $row->related_url1 .'">'. $row->related_url1 .'</a></p>';
      }
      if ($row->related_url2) {
        $comment['comment'] .= '<p><a href="'. $row->related_url2 .'">'. $row->related_url2 .'</a></p>';
      }
      if ($row->parent_class == "Comment") {
        $comment['pid'] = $this->getCommentCid($row->parentid, "comments");
      }

      // Save the comment.
      $cid = comment_save($comment);
      // Sadly the comment_save function sets timestamp to time()...
      $newtime = strtotime($row->created_datetime);
      db_query("UPDATE {comments} SET timestamp=%d WHERE cid=%d", array($newtime, $cid));
      
      $record = array('objectid' => $row->objectid, 'cid' => $cid);
      dadaimcpath_insert($nid, $row->objectid, $cid, 'comments');

      //hide comments
      $query = 'SELECT * FROM objections WHERE refid=%d AND type="Comment"';
      $rsc = $this->dbQuery($query, $row->objectid);
      while ($objection = mysql_fetch_object($rsc)) {
        if ($objection) {
          $hidden = new StdClass();
          $hidden->nid = $comment->nid;
          $hidden->cid = $cid;
          $hidden->created = strtotime($objection->modified_timestamp);
          $hidden->rid = 1;
          $hidden->publicnote = $objection->hide_code .": ". $objection->hide_notes;
          $hidden->uid = 2;
          $hidden->delay = 0;

          drupal_write_record('hidden_comment', $hidden);
          db_query("UPDATE {comments} SET status = 1 WHERE cid=%d", array($cid));
        }
      }
      }
      $this->updateContext();
    }
  }

  function class_to_identifier($class) {
    switch ($class) {
        case 'Article': $type = 'newswire'; break;
        case 'OtherPress': $type = 'otherpress'; break;
        case 'Comment' : $type = 'comments'; break;
        case 'Feature' : $type = 'feature'; break;
        case 'Media' : $type = 'media'; break;
      }
//      watchdog('dadaimc2drupal', t("root_class: %type"), array("%type"=>$type), WATCHDOG_DEBUG);
    return $type;
  }


}
