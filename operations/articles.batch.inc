<?php
// $Id: articles.batch.inc,v 1.1.2.2 2010/11/15 00:09:19 jonhattan Exp $

require_once 'base.batch.inc';

/**
 * Batch operation. Import dadaIMC articles as story nodes.
 *
 * Fields not imported:
 modified_by 
 edit_datetime 
 language 
 mime_type
 media
 submitted
 rating
 embedded_media
 local_interest
 thumbnail
 license
 comment_notification
 revision
 */
class dadaImportArticles extends BaseBatchOperation implements iBatchOperation {
  public $token = 'articles';

  function getTotal() {
    $query = 'SELECT COUNT(objectid) FROM articles WHERE deleted=0 and objectid < 40000 and objectid > 35000';
    $query = 'SELECT COUNT(objectid) FROM articles WHERE deleted=0';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $query = "SELECT * FROM articles WHERE deleted=0 and objectid < 40000 and objectid > 35000 LIMIT $current, ".$this->step;
    $query = "SELECT * FROM articles WHERE deleted=0 LIMIT $current, ".$this->step;
    //$query = "SELECT related_url1, related_url2, objectid, created_datetime, edit_datetime, heading, displayable, modified_timestamp, feature, authorid, summary, body, section, author FROM articles WHERE deleted=0 and objectid < 40000 and objectid > 35000 LIMIT $current, ".$this->step;
    //$query = "SELECT related_url1, related_url2, objectid, created_datetime, edit_datetime, heading, displayable, modified_timestamp, feature, authorid, summary, body, section, author FROM articles WHERE deleted=0 LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      if ($row->objectid == 36841) {
        //watchdog("dimc2d_iconv", $row->body, array(), WATCHDOG_DEBUG);
        //watchdog("dimc2d_iconv", $this->encode($row->body), array(), WATCHDOG_DEBUG);
        //watchdog("dimc2d_iconv", mb_convert_encoding($row->body), array(), WATCHDOG_DEBUG);
        watchdog("dimc2d_iconc", utf8_encode($row->body), array(), WATCHDOG_DEBUG);
        //watchdog("dimc2d_iconv", $row->summary, array(), WATCHDOG_DEBUG);
        //watchdog("dimc2d_iconv", $this->encode($row->summary), array(), WATCHDOG_DEBUG);
        //watchdog("dimc2d_iconv", mb_convert_encoding($row->summary), array(), WATCHDOG_DEBUG);
        watchdog("dimc2d_iconc", utf8_encode($row->summary), array(), WATCHDOG_DEBUG);
      }
      $feature = false;

      $fquery = 'SELECT * from features where ref_class="Article"';
      $fres = $this->dbQuery($fquery);
      while ($frow = mysql_fetch_object($fres)) {
        if ($frow->refid == $row->objectid) {
          $feature = true;
          $row->featureid = $frow->objectid;
          $row->summary = $frow->summary;
        }
      }
      if (!$feature) {
        $node = $this->saveArticle($row, "Article");

        $this->add_media_to_field(&$node, $row->objectid, $row->embedded_media);
        dadaimcpath_insert($node->nid, $row->objectid, NULL, 'newswire');
      } else {
        $node = $this->saveArticle($row, "Feature");
        $this->add_media_to_field(&$node, $row->objectid, $row->embedded_media);
        dadaimcpath_insert($node->nid, $row->objectid, NULL, 'newswire');
        dadaimcpath_insert($node->nid, $row->featureid, NULL, 'feature');
      }
      $this->updateContext();
    }
  }
}
