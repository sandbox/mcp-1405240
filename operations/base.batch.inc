<?php
// $Id: base.batch.inc,v 1.1.2.2 2010/11/15 00:09:19 jonhattan Exp $

interface iBatchOperation {
  public function getTotal();
  public function process($current, $total);
}

class Database {
  function __construct() {
    $dbhost = variable_get('dadaimc2drupal_dbhost', NULL);
    $dbuser = variable_get('dadaimc2drupal_dbuser', NULL);
    $dbpass = variable_get('dadaimc2drupal_dbpass', NULL);
    $dbname = variable_get('dadaimc2drupal_dbname', NULL);
    $this->dbConnect($dbhost, $dbuser, $dbpass, $dbname);  
  }
  
  /**
   * Stablish a connection to the database.
   */
  function dbConnect($dbhost, $dbuser, $dbpass, $dbname) {
    $link = @mysql_connect($dbhost, $dbuser, $dbpass, TRUE, 2);
    if (!$link) {
      drupal_set_message(t('Unable to connect to database at «%host» with user «%user» and password «%pass».', array('%host' => $dbhost, '%user' => $dbuser, '%pass' => $dbpass)), 'error');
    }
    elseif (!mysql_select_db($dbname)) {
        drupal_set_message(t('Unable to select database :db.', array(':db' => $dbname)), 'error');
        $link = FALSE;
    }
    if (!$link) {
      form_set_error('', t("Unable to stablish a connection to DadaIMC database. Review the settings in the <a href='@url'>database configuration page</a>.", array('@url'=>url('admin/settings/dadaimc2drupal'))));
    }
    //mysql_query('SET NAMES "utf8"', $link);

    $this->link = $link;
  }

  /**
   * Executes a query on the database.
   *
   * The query is in drupal format, that is, you can use {} around
   * the table names.
   */
  function dbQuery($query) {
    $args = func_get_args();
    array_shift($args);
    // 'All arguments in one array' syntax?
    if (isset($args[0]) and is_array($args[0])) {
      $args = $args[0];
    }
    _db_query_callback($args, TRUE);
    $query = preg_replace_callback(DB_QUERY_REGEXP, '_db_query_callback', $query);
    $query = strtr($query, array('{' => $this->dbPrefix, '}' => ''));
    $result = mysql_query($query, $this->link);

    if (!mysql_errno($this->link)) {
      return $result;
    }
    else {
      // Indicate to drupal_error_handler that this is a database error.
      ${DB_ERROR} = TRUE;
      trigger_error(check_plain(mysql_error($this->link) ."\nThe query was: ". $query), E_USER_WARNING);
      return FALSE;
    }
  }
}

class BaseBatchOperation extends Database {
  static function execute(&$context, $step = 20) {
     $class = get_called_class();
     $operation = new $class($context, $step);
  }

  function __construct(&$context, $step) {
    // Initialize database connection.
    parent::__construct();

    // Initialize batch context.
    $this->context = &$context;
    $this->step = $step;
    // If it is the first time, initialize $context.
    if (empty($context['sandbox'])) {
      $context['sandbox']['total'] = $this->getTotal();
      $context['sandbox']['current'] = 0;
      if ($context['sandbox']['total'] == 0) {
        $context['finished'] = 1;
        return;
      }
    }

    // Execute the operation.
    $this->process($context['sandbox']['current'], $context['sandbox']['total']);

    // Update finished value 0..1
    $context['finished'] = $context['sandbox']['current'] / $context['sandbox']['total'];
  }

  /**
   * Update the $context upon each iteration of this operation.
   *
   * This function is to be called at the end of each iteration in the operation loop.
   */
  function updateContext() {
    $this->context['results'][$this->token]++;
    $this->context['sandbox']['current']++;
    $current = $context['sandbox']['current'];
    $this->context['message'] = t('Importing @token @current of @total.', array(
      '@token' => $this->token,
      '@current' => $this->context['sandbox']['current'],
      '@total' => $this->context['sandbox']['total']
      ));
  }

  /**
   * Obtain the drupal uid for a dada user.
   *
   * Use a internal cache as presumibly this function can be called
   * several times in the same batch iteration.
   *
   * If user doesn't exist return 0.
   */
  function getUid($objectid) {
    static $users;
    if (!isset($users[$objectid])) {
      $query = "SELECT uid FROM dada_users WHERE objectid=%d";
      $uid = db_result(db_query($query, $objectid));
      if ($uid === FALSE) {
        $uid=0;
      }
      $users[$objectid] = $uid;
    }

    return $users[$objectid];
  }

  /**
   * Obtain the tid for a dada category.
   *
   * Use a internal cache as presumibly this function can be called
   * several times in the same batch iteration.
   */
  function getTerm($objectid) {
    static $terms;
    if (!isset($terms[$objectid])) {
      $query = "SELECT tid FROM dada_categories WHERE objectid=%d";
      $tid = db_result(db_query($query, $objectid));
      $terms[$objectid] = $tid;
    }

    return $terms[$objectid];
  }

  /**
   * Obtain the nid for a dada article or feature.
   *
   * No need for internal cache. 
   */
  function getArticleNid($objectid, $type) {
//    $query = "SELECT nid FROM dada_articles WHERE objectid=%d";
    $query = 'SELECT nid FROM dadaimcpath WHERE objectid=%d AND type="%s"';
    $nid = db_result(db_query($query, array($objectid, $type)));

    return $nid;
  }
  function getCommentCid($objectid) {
    $query = 'SELECT cid FROM dadaimcpath WHERE objectid=%d AND type="comments"';
    $cid = db_result(db_query($query, array($objectid)));
    return $cid;
  }
  
  /**
   *
   */
  function saveArticle(&$row, $type) {
    if (($row->heading == "") &&
      ($row->summary == "") &&
      ($row->body == "")
    ) return FALSE;

    $created_datetime = strtotime($row->created_datetime);
    if (!$created_datetime) {
      $created_datetime = strtotime($row->edit_datetime);
    }

    if ($type == 'Feature') {
      $nodetype = 'dadafeature';
    } else {
      if ($row->feature == 1) {
        $nodetype = 'dadafeature';
      } else {
        $nodetype = 'dadaarticle';
      }
    }
    if ($type == "Otherpress") {
      $nodetype = 'dadaotherpress';
    }
    
    $body = $this->encode($row->body, $row->objectid);
    $teaser = $this->encode($row->summary, $row->objectid);
    $title = $this->encode($row->heading, $row->objectid);
    $node = (object)array(
      'type' => $nodetype,
      'title' => str_replace("&quot;", "\"", $title),
      'teaser' => $teaser,
      'body' => $teaser . ($body ? "<!--break-->\n\n". $body : ''),
      'status' => $row->displayable,
      'created' => $created_datetime,
      'changed' => strtotime($row->modified_timestamp),
      'promote' => 0,
      'comment' => 1, // (1 = no further comments)
      // we use input format 6 for html
      'format' => 6,  // Full HTML
      // uid is 0 at at.indymedia.org
      'uid' => 0,
    );
    
    // #TODO# Related urls.
    /*if ($row->related_url1 || $row->related_url2) {
      $node->body .= '<br /><p><strong>Related:</strong></p>';
    }
    if ($row->related_url1) {
      $node->body .= '<p><a href="'. $row->related_url1 .'">'. $row->related_url1 .'</a></p>';
    }
    if ($row->related_url2) {
      $node->body .= '<p><a href="'. $row->related_url2 .'">'. $row->related_url2 .'</a></p>';
    }
    if ($type == "Otherpress") {
      $url = array('url' => $row->link, 'title' => '', 'attributes' => '');
      $node->field_link_to_otherpress[] = $url;
    }*/
	if ($type == 'Otherpress') {
		$url = $this->encode($row->link);
		$title = $this->encode($row->organization);
		$node->field_link_to_otherpress[0] = array('url' => $url, 'title' => $title, 'attribute' => array());
	} else {
	if ($row->related_url1) {
	    	$url1 = $this->encode($row->related_url1);
		$node->field_related_urls[0] = array('url' => $url1, 'title' => NULL, 'attributes' => array());
	}
	if ($row->related_url2 && $row->related_url1) {
		$url2 = $this->encode($row->related_url2);
		$node->field_related_urls[1] = array('url' => $url2, 'title' => NULL, 'attributes' => array());
	}
	if ($row->related_url2 && !$row->related_url1) {
		$url1 = $this->encode($row->related_url2);
		$node->field_related_urls[0] = array('url' => $url1, 'title' => NULL, 'attributes' => array());
	}
	}
    //$url2 = $this->encode($row->related_url2);
//    $name = array('value' => check_plain($row->author));
	$name = $this->encode($row->author);
    //$name = array('value' => $row->author);
    //$node->field_name[] = check_plain($row->author);
    $node->field_name[] = array('value' => $name);

    node_save($node);
    if ($node->status == 1) {
      $node->workflow = 12;
    }
    node_save($node);
    if (!$node->nid) {
      return FALSE;
    } /*else {
      if ($nodetype == 'indy_feature') {
        $workflow = new StdClass();
        $workflow->nid = $nid;
        $workflow->sid = 12;
        $workflow->uid = 2;
        $workflow->stamp = time();
        drupal_write_record('workflow_node', $workflow, 'nid');
      }
    }*/
    $flag = flag_get_flag('dadaimported') or die('no "dadaimported" flag defined');

    // Flag node #456:
    $flag->flag('flag', $node->nid); 
    $query = "UPDATE {node} SET changed=%d WHERE nid=%d";
    $newtime = strtotime($row->modified_timestamp);
    db_query("UPDATE {node} SET changed=%d WHERE nid=%d", array($newtime, $node->nid));
    // #TODO# anonymous attribution.
    /*
    if (($uid == 0) && ($row->author)) {
      db_query('INSERT INTO {nodextradata} (nid, author) VALUES (%d,"%s")', $node->nid, $row->author);
    }
    */

    // Assign terms.
    // Dada categories.
    $terms = array();
    $query = 'SELECT DISTINCT category_id FROM hash_category WHERE ref_class="%s" AND ref_id=%d';
    $rsc = $this->dbQuery($query, $type, $row->objectid);
    while ($cat = mysql_fetch_object($rsc)) {
      $terms[] = $this->getTerm($cat->category_id);
    }
    // Dada section.
    /* TODO: section for Otherpress and Media ? */
    //$sections = variable_get('dadaimc2drupal_sections_terms', array());
    /*if (isset($sections[$row->section])) {
      $terms[] = $sections[$row->section];
    }*/
    taxonomy_node_save($node, $terms);

    /* hide the node */
    if ($node->status == 0) {
      watchdog('dimc2d_hide', t("hiding dada node %nid"), array('%nid' => $node->nid), WATCHDOG_DEBUG);
      $query = 'SELECT * FROM objections WHERE refid=%d AND type="%s"';
      $rsc = $this->dbQuery($query, $row->objectid, $type);
      while ($objection = mysql_fetch_object($rsc)) {
        $hidden = new StdClass();
        $hidden->nid = $node->nid;
        //$hidden->created = time();
        $hidden->created = strtotime($objection->modified_timestamp);
        $hidden->rid = 1;
        $hidden->publicnote = $objection->hide_code .": ". $objection->hide_notes;
        $hidden->uid = 2;
        $hidden->delay = 0;

        drupal_write_record('hidden_node', $hidden);
      }
    }

    return $node;
  }

function encode($str, $objectid = 0) {
  //if ($this->islatin1($str)) {
  /*if (false === mb_check_encoding($str, "UTF-8")) {
    if (false === mb_check_encoding($str, "ASCII")) {*/
   /*if ($objectid == 36841) {
     watchdog('dimc2d_enc', t($str), array(), WATCHDOG_DEBUG);
   }*/
   if ((mb_detect_encoding($str) != "UTF-8") && (mb_detect_encoding($str) != "ASCII")) {
      $enc = mb_detect_encoding($str);
      /*if ($objectid == 36841) {
        watchdog('dimc2d_enc', t("encoding: %enc oid: %oid"), array('%enc' => $enc, '%oid' => $objectid), WATCHDOG_DEBUG);
      }*/
      $strcon = mb_convert_encoding($str, "UTF-8");
      //$strcon = utf8_encode($str);
      //return $this->ascii2entities($str);
      //return iconv("ISO-8859-1", "UTF-8//TRANSLIT", $str);
      /*if ($objectid == 36841) {
        watchdog('dimc2d_enc', t($strcon), array(), WATCHDOG_DEBUG);
      }*/
    //return(iconv("ISO-8859-1", "UTF-8//TRANSLIT",$strcon));
      if ($objectid==36841) {
        watchdog('dimc2d_enc', t("in the if clause", array(), WATCHDOG_DEBUG));
      }
    return $strcon;
  } else {
    if (mb_detect_encoding($str) == "ASCII") {
      //return utf8_encode($str);
      if ($objectid==36841) {
        watchdog('dimc2d_enc', t("in the else -> if", array(), WATCHDOG_DEBUG));
      }
      //$strcon = iconv("ASCII", "UTF-8//TRANSLIT", $str);
      $strcon = $str;
    } else {
      if ($objectid==36841) {
        watchdog('dimc2d_enc', t("in the else -> else", array(), WATCHDOG_DEBUG));
      }
      if (false === mb_check_encoding($str, "UTF-8")) {
        if ($objectid==36841) {
          watchdog('dimc2d_enc', t("in the else -> else -> if", array(), WATCHDOG_DEBUG));
        }
        //$strcon = iconv("UTF-8", "UTF-8//TRANSLIT", $str);
        //$strcon = mb_convert_encoding($str, "UTF-8");
        $strcon = utf8_encode($str);
      } else {

        $strcon = $str;
      }
    } 
    return $strcon;
  }
}
  /**
   *
   */
  function register_node_media($node, &$minfo) {
    foreach ($minfo as $m) {
      $path = dadaimc2drupal_filepath($row->mime_class, $row->filename);
      if (!file_exists(dadaimc2drupal_uploadpath($path))) {
        continue;
      }
      if (strncmp('image/', $row->mime_type, 6) === 0) {
        // XXX what about large image size?...
        $file = array(
          'uid' => $node->uid,
          'filename' => basename($path),
          'filepath' => dadaimc2drupal_uploadpath($path),
          'filemime' => $row->mime_type,
          'filesize' => filesize(dadaimc2drupal_uploadpath($path)),
          'status' => FILE_STATUS_PERMANENT,
          'timestamp' => $node->created,
        );
        drupal_write_record('files', $file);
        $upload = array(
          'fid' => $file['fid'],
          'nid' => $node->nid,
          'vid' => $node->vid,
          'description' => '',
          'list' => 0,
          'weight' => 0,
        );
        drupal_write_record('upload', $upload);

        $parent_fid = $file['fid'];
        $thumb = dadaimc2drupal_filepath_thumbnail($row->mime_class, $row->filename);
        if (file_exists(dadaimc2drupal_uploadpath($thumb))) {
          $file = array(
            'uid' => $node->uid,
            'filename' => '_thumbnail',
            'filepath' => dadaimc2drupal_uploadpath($thumb),
            'filemime' => $row->mime_type,
            'filesize' => filesize(dadaimc2drupal_uploadpath($thumb)),
            'status' => FILE_STATUS_PERMANENT,
            'timestamp' => $node->created,
          );
          drupal_write_record('files', $file);
          $upload_deriv = array(
            'fid' => $file['fid'],
            'fid_parent' => $parent_fid,
            'type' => 'img.thumb',
          );
          drupal_write_record('upload_derivative', $upload_deriv);
        }
        $big = dadaimc2drupal_filepath_large($row->mime_class, $row->filename);
        if (file_exists(dadaimc2drupal_uploadpath($big))) {
          $file = array(
            'uid' => $node->uid,
            'filename' => '_large',
            'filepath' => dadaimc2drupal_uploadpath($big),
            'filemime' => $row->mime_type,
            'filesize' => filesize(dadaimc2drupal_uploadpath($big)),
            'status' => FILE_STATUS_PERMANENT,
            'timestamp' => $node->created,
          );
          drupal_write_record('files', $file);
          $upload_deriv = array(
            'fid' => $file['fid'],
            'fid_parent' => $parent_fid,
            'type' => 'img.large',
          );
          drupal_write_record('upload_derivative', $upload_deriv);
        }
      }
      else {
        $file = array(
          'uid' => $node->uid,
          'filename' => basename($path),
          'filepath' => dadaimc2drupal_uploadpath($path),
          'filemime' => $row->mime_type,
          'filesize' => filesize(dadaimc2drupal_uploadpath($path)),
          'status' => FILE_STATUS_PERMANENT,
          'timestamp' => $node->created,
        );
        drupal_write_record('files', $file);
        $upload = array(
          'fid' => $file['fid'],
          'nid' => $node->nid,
          'vid' => $node->vid,
          'description' => '',
          'list' => 1,
          'weight' => 0,
        );
        drupal_write_record('upload', $upload);
      }
    }
  }

  /**
   *
   */
  function image_insert($node, $embedded_media_type, $imgtag, $img_seq_no) {
    switch ($embedded_media_type) {
      case 'aftersummary':
        if ($img_seq_no == 1) {
          $node->body = $imgtag . $node->body;
          $node->teaser = $imgtag . $node->teaser;
        }
        break;
      case 'oneaftersummary':
        if ($img_seq_no == 1) {
          $node->body = $imgtag . $node->body;
          $node->teaser = $imgtag . $node->teaser;
        }
        /*else {
          $body .= $imgtag;
          $teaser .= $imgtag;
        }*/
        break;
      case 'placeholders':
        $node->body = str_replace("#file_". $img_seq_no ."#", $imgtag, $node->body);
        $node->teaser = str_replace("#file_".$img_seq_no ."#", $imgtag, $node->teaser);
        break;
      default:
        //$body .= $imgtag;
        break;
    }
    
    node_save($node);
  }
  
  /**
   * From dadaimc. Used to sortof hash media files into directories 1..13.
   */
  function dadaimc2drupal_subdir($filename) {
    // gets a number between 1 and 13 using the first character
    $num = 0;
    for ($x = 0; $x < strlen($filename); $x++) {
      $num += ord($filename[$x]);
    }
    return ($num % 13) + 1;
  }

  /**
   *
   */
  function dadaimc2drupal_urlpath($path) {
    return '/'. $this->file_directory_path() .'/usermedia/'. $path;
  }

  /**
   *
   */
  function dadaimc2drupal_uploadpath($path) {
    return $this->file_directory_path() .'/usermedia/'. $path;
  }

  /**
   *
   */
  function dadaimc2drupal_filepath($mime_class, $filename) {
    return $mime_class .'/'. $this->dadaimc2drupal_subdir($filename) .'/'. $filename;
  }

  /**
   *
   */
  function dadaimc2drupal_filepath_thumbnail($mime_class, $filename) {
    return $mime_class .'/'. $this->dadaimc2drupal_subdir($filename) .'/thumb/'. $filename;
  }

  /**
   *
   */
  function dadaimc2drupal_filepath_large($mime_class, $filename) {
    return $mime_class .'/'. $this->dadaimc2drupal_subdir($filename) .'/large/'. $filename;
  }
  function file_directory_path() {
    return variable_get('dadaimc2drupal_path', NULL);
  }
  
  /* add media to an file/imagefield
   * id is either objectid of media or id of parentarticle
   * aftersummary is 'media' if media
   */
  function add_media_to_field(&$node, $id, $aftersummary = '') {
    if ($aftersummary == 'media') {
      $sql = "SELECT * FROM media WHERE objectid=%d AND filename <> '' ORDER BY sequence_number %s";
    } else {
      $sql = "SELECT * FROM media WHERE parentid=%d AND filename <> '' ORDER BY sequence_number %s";
    }
    $mq = $this->dbQuery($sql, $id, ($aftersummary == 'aftersummary' ? "DESC" : "ASC"));
    if ($mq) 
      $seqnr = 0;
      while ($media = mysql_fetch_array($mq)) {
        $seqnr++;
        //get absolute path
        $fpabs = $this->dadaimc2drupal_uploadpath($this->dadaimc2drupal_filepath($media['mime_class'], $media['filename']));
        $fpabsthumb = $this->dadaimc2drupal_uploadpath($this->dadaimc2drupal_filepath_thumbnail($media['mime_class'], $media['filename']));
        $fpabslarge = $this->dadaimc2drupal_uploadpath($this->dadaimc2drupal_filepath_large($media['mime_class'], $media['filename']));

        //TODO: ueberlegen ob das sinnvoll ist
        if (file_exists($fpabs)) {
          $filepath = "files/usermedia/".$this->dadaimc2drupal_filepath($media['mime_class'], $media['filename']);
        } else {
          if (file_exists($fpabslarge)) {
            $filepath = "files/usermedia/".$this->dadaimc2drupal_filepath_large($media['mime_class'], $media['filename']);
          } else {
            if (file_exists($fpabsthumb)) {
              $filepath = "files/usermedia/".$this->dadaimc2drupal_filepath_thumbnail($media['mime_class'], $media['filename']);
            } else {
              unset($filepath);
            }
          }
        }

        if (isset($filepath)) {
          $timestamp = strtotime($media['created_datetime']);
          $file = $this->add_existing_file($filepath, $timestamp, 0);

  //        watchdog('dadaimc2drupalimg', t("linking %filename to node %nid"), array('%filename' => $file->filename, '%nid' => $node->nid), WATCHDOG_DEBUG, l(t('view'), 'node/' . $node->nid));

          if ($media['mime_class'] == 'image') {
            $imagecache = preg_replace("/^files/", "files/imagecache/article_thumbs", $filepath);
            $this->add_image_to_image_field(&$node, &$file, $this->encode($media['orig_filename']), $this->encode($media['caption']), $id);
            $imagetag = '<img src="/'.$imagecache.'" style="float:left">';
            if ($id == 35876) {
              watchdog('dimc2d_imgi', t("inserting %af %im %sn"), array('%af'=>$aftersummary,'%im' => $imagetag, '%sn' => $seqnr ), WATCHDOG_DEBUG);
            }
            $this->image_insert(&$node, $aftersummary, $imagetag, $seqnr);
            node_save($node);
          } else {
            if ($media['mime_class'] == 'audio' | $media['mime_class'] == 'video') { 
              $this->add_file_to_media_field(&$node, &$file);
            } else {
              $this->add_file_to_file_field(&$node, &$file);
            }
          }
        }
    }
  }

  function add_image_to_image_field(&$node, &$file, $alt = '', $title = '', $id) {

    $file['list'] = 1;
    $file['data'] = array('alt'=> $alt, 'title' => $title);
    if ($node->type == 'indy_feature') {
      $node->field_images_0[] = $file;
    } else {
      $node->field_picture[] = $file;
    }
    node_save($node);
  }

  function add_file_to_file_field(&$node, &$file) {
    $node->field_attach_file[] = $file;
    node_save($node);
  }

  function add_file_to_media_field(&$node, &$file) {
    $node->field_media[] = $file;
    node_save($node);
  }

  function add_existing_file($file_path, $timestamp, $uid=1) { 
    $file = (object)array(
      'filename' => basename($file_path),
      'filepath' => $file_path,
      'filemime' => file_get_mimetype($file_path),
      'filesize' => filesize($file_path),
      'origname' => basename($file_path),
      'uid' => $uid,
      'status' => FILE_STATUS_PERMANENT,
      'timestamp' => $timestamp
    );
  drupal_write_record('files', $file);
  return field_file_load($file_path);
  }

  function dadaimc2drupal_addpathalias($objectid, $type, $nid, $cid = NULL) {
      switch ($type) {
	  case "article":
	      path_set_alias('node/' . $nid, 'newswire/display/' . $objectid . '/index.php');
        break;
	  case "feature":
	      path_set_alias('node/' . $nid, 'feature/display/' . $objectid . '/index.php');
        break;
	  case "otherpress":
	      path_set_alias('node/' . $nid, 'mod/otherpress/display/' . $objectid . '.html');
        break;
	  case "media":
	      path_set_alias('node/' . $nid, 'media/all/display/'. $objectid . '/index.php');
        break;
	  case "comment":
	      path_set_alias('node/' . $nid . '#comment-' . $cid, 'mod/comments/display/' . $objectid);
        break;
      }
  }

  function mywdog($str, $id) {
    if ($id == 35971) {
      watchdog('dimc2dwd', t($str), array(), WATCHDOG_DEBUG);
    }
  }
  function ascii2entities($string){
    for($i=128;$i<=255;$i++){
        $entity = htmlentities(chr($i), ENT_QUOTES, 'cp1252');
        $temp = substr($entity, 0, 1);
        $temp .= substr($entity, -1, 1);
        if ($temp != '&;'){
            $string = str_replace(chr($i), '', $string);
        }
        else{
            $string = str_replace(chr($i), $entity, $string);
        }
    }
    return $string;
  }
}
