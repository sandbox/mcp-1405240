<?php
// $Id: test.batch.inc,v 1.1.2.2 2010/12/13 21:34:24 jonhattan Exp $

require_once 'base.batch.inc';

/**
 * Example batch operation.
 */
class dadaImportCparent extends BaseBatchOperation implements iBatchOperation {
  public $token = 'cparent';

  function getTotal() {
    $query = 'SELECT count(objectid) FROM comments WHERE deleted = 0 AND parent_class="Comment"';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;

  }
  
  function process($current, $total) {
//    watchdog('dadaimc2drupal', t("Cparent current: %current, total: %total"), array("%current"=>$current, "%total"=>$total), WATCHDOG_DEBUG);
    $query = "SELECT objectid,parentid FROM comments WHERE deleted = 0 AND parent_class=\"Comment\" LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      if ($row->objectid == 123303) {
        watchdog('dadaimc2drupal', t("Hurray 123303"), array(), WATCHDOG_DEBUG);
      }
      // do something
      $pid = $this->getCommentCid($row->parentid, "comments");
      $cid = $this->getCommentCid($row->objectid, "comments");
      db_query('UPDATE {comments} SET pid=%d WHERE cid=%d', array($pid, $cid));

      $this->updateContext();
    }
  }
}
