<?php
// $Id: objections.batch.inc,v 1.1.2.1 2010/11/13 14:14:17 jonhattan Exp $

require_once 'base.batch.inc';

/**
 * Batch operation.
 */
class dadaImportObjections extends BaseBatchOperation implements iBatchOperation {
  public $token = 'objections';

  function getTotal() {
    $query = 'SELECT COUNT(*) FROM objections WHERE hide=1 AND (type="Article" OR type="Comment" OR type="Media")';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }

  function process($current, $total) {
    $query = 'SELECT * FROM objections WHERE hide=1 AND (type="Article" OR type="Comment" OR type="Media") ORDER BY modified_timestamp DESC LIMIT '.$current.','.$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      if ($row->type == 'Media') {
        $table = 'dada_media';
      }
      else {
        $table = 'dada_'.strtolower($row->type).'s';
      }
    $nid = db_result(db_query('SELECT nid FROM '.$table.' WHERE objectid = %d', $row->refid));
    if (!$nid) {
      $this->context['sandbox']['current']++;
      continue;
    }
    // there can be objections duplicates due to the way dada stored them...
    if (db_result(db_query('SELECT COUNT(*) FROM {imc_node_moderation} WHERE nid = %d', $nid))) {
      $this->context['sandbox']['current']++;
      continue;
    }

    // featured articles just drop hidden info. it is usually users voting it down but the effect is silly
    if (db_result(db_query('SELECT COUNT(*) FROM {node} WHERE nid = %d AND promote = 1', $nid))) {
      $this->context['sandbox']['current']++;
      continue;
    }

    $reason = ($row->hide_code ? $row->hide_code .": ". $row->hide_notes : $row->hide_notes);

    db_query("INSERT INTO {imc_node_moderation} (nid, status, comment, uid, timestamp) VALUES (%d,%d,'%s',%d,%d)",
      $nid, IMCEDITOR_NODE_STATUS_HIDDEN, $reason, 1, strtotime($row->modified_timestamp)
    );
    db_query("UPDATE {node} SET status=0 WHERE nid=%d", $nid);

    $this->updateContext();
    }
  }
}
