<?php
// $Id: otherpress.batch.inc,v 1.1.2.1 2010/11/13 14:14:17 jonhattan Exp $

require_once 'base.batch.inc';

/**
 * Batch operation. Import dadaIMC otherpress as ...
 */
class dadaImportOtherpress extends BaseBatchOperation implements iBatchOperation {
  public $token = 'otherpress';

  function getTotal() {
    $query = 'SELECT COUNT(*) FROM otherpress WHERE deleted = 0';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $query = "SELECT * FROM otherpress WHERE deleted = 0 LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $row->displayable = 1;
      $row->feature = 0;
      $row->body = "";
      $row->related_url1 = $row->link;
      $row->related_url2 = "";
      
//      $node = dadaimc2drupal_save_article(&$dbArgs, $article, $connection, "Otherpress");
      $node = $this->saveArticle($row, "Otherpress");
/*      if ($node->nid) {
        $record = array('objectid' => $row->objectid, 'nid' => $node->nid);
        drupal_write_record('dada_otherpress', $record);
      }*/

      watchdog('dadaimc2drupal', t("node: %node -> otherpress: %oid"), array("%node"=>$node->nid, "%oid"=>$row->objectid), WATCHDOG_DEBUG);
      $this->add_media_to_field(&$node, $row->objectid, 'foo');
      dadaimcpath_insert($node->nid, $row->objectid, NULL, 'otherpress');
      $this->updateContext();
    }
  }
}
