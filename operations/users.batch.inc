<?php
// $Id: users.batch.inc,v 1.1.2.2 2010/11/15 00:09:19 jonhattan Exp $

require_once 'base.batch.inc';

/**
 * Batch operation. Import dadaIMC users as drupal users.
 *
 * Fields not imported:
 * modified_timestamp  | modified_by | deleted | old_lastaccess_timestamp | chash                            | fullname   | firstname | lastname | phone | address | address2 | city | state | zip | country | email_verified | set_cookie | language | stylesheet | default_section | default_category | is_public | pw_alt_email          | homepage             | chat_client | chat_id              | organizations | biography | photo |
 */
class dadaImportUsers extends BaseBatchOperation implements iBatchOperation {
  public $token = 'users';

  function getTotal() {
    $query = 'SELECT COUNT(objectid) FROM users';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }

  function process($current, $total) {
    $query = "SELECT objectid, username, email, password, pw_method, created_datetime, lastaccess_timestamp, level FROM users LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $uid = $this->userExist($row->username);
      if ($uid === FALSE) {
        // Ignore users with pre-MD5 password hash or empty email.
        if ((strtoupper($row->pw_method) != 'MD5') || (!$row->email)) {
          $this->context['sandbox']['current']++;
          continue;
        }
        // Some old dada users don't have valid last access timestamp.
        if ($row->lastaccess_timestamp == '0000-00-00 00:00:00') {
          $last_access = strtotime($row->created_datetime);
        }
        else {
          $last_access = strtotime($row->lastaccess_timestamp);
        }
        // Roles. Editor or Admin.
        $roles = array();
        if ($row->level != 'User') {
          $roles[] = variable_get('dadaimc2drupal_roles_'.strtolower($row->level), NULL);
        }
        //
        $account = array(
          'name' => $row->username,
          'pass' => $row->password,
          'created' => strtotime($row->created_datetime),
          'access' => $last_access,
          'login' => $last_access,
          'status' => 1,
          'roles' => $roles
        );
        user_save('', $account);
        // user_save likes to store the md5sum of the password provided,
        // but actually we already have that so let's put it in the db.
        db_query("UPDATE {users} SET pass='%s' WHERE uid=%d", $row->password, $account['uid']);
      }
      // Store the mapping.
      $record = array('objectid' => $row->objectid, 'uid' => $uid);
      drupal_write_record('dada_users', $record);

      $this->updateContext();
    }
  }

  /**
   * Returns the uid for $name or FALSE.
   */
  function userExist($name) {
    $rsc = db_query("SELECT uid FROM {users} u WHERE name = '%s'", array($name));
    if ($user = db_fetch_object($rsc)) {
      return $user->uid;
    }
    return FALSE;
  }
}

