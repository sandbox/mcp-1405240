<?php
// $Id: media.batch.inc,v 1.1.2.2 2010/11/15 00:09:19 jonhattan Exp $

require_once 'base.batch.inc';

/**
 * Batch operation. Import dadaIMC media as ....
 */
class dadaImportMedia extends BaseBatchOperation implements iBatchOperation {
  public $token = 'media';

  function getTotal() {
    $query = "SELECT COUNT(*) FROM media WHERE parent_class='' AND deleted=0";
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }

  function process($current, $total) {
    $query = "SELECT * FROM media WHERE parent_class='' AND deleted=0 LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      if ($row->caption) {
        if (strlen($row->caption) > 0) {
          $row->heading = $row->caption;
        }
        else if (strlen($row->caption) > 64) {
          $row->heading = substr($row->caption, 0, 64) .'...';
        }
        //$row->heading = $row->orig_filename .': '. $row->heading;
      }
      else {
        $row->heading = $row->orig_filename;
      }
      $row->summary = '';
      $row->body = '';
      $row->section = '';
      $row->embedded_media = '';
      $row->authorid = '';
      $row->author = ($row->contributor!='')?$row->contributor:$row->artist;
      $row->related_url2 = '';

//      dadaimc2drupal_article_add_media(&$row, &$m);

//      $node = $this->saveArticle($row, "");
      $node = $this->saveArticle($row, "Media");
      watchdog('dadaimc2drupal', t("node: %node -> media: %oid"), array("%node"=>$node->nid, "%oid"=>$row->objectid), WATCHDOG_DEBUG);

      $this->add_media_to_field(&$node, $row->objectid, 'media');
      /*if (is_object($node)&&($node->nid)) {
        //$minfo = array($m);
        //dadaimc2drupal_register_node_media($node, &$minfo);
        $record = array('objectid' => $row->objectid, 'nid' => $node->nid);
        drupal_write_record('dada_media', $record);        
      }*/
      dadaimcpath_insert($node->nid, $row->objectid, NULL, 'media');
      $this->updateContext();
    }
  }
}
