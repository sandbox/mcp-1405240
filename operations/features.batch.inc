<?php
// $Id: features.batch.inc,v 1.1.2.2 2010/11/15 00:09:19 jonhattan Exp $

require_once 'base.batch.inc';

/**
 * Batch operation. Import dadaIMC features as nodes.
 *
 * Fields not imported:
 * modified_by | language |  mime_type | embedded_media | thumbnail | submitted | rating  | license |
 */
class dadaImportFeatures extends BaseBatchOperation implements iBatchOperation {
  public $token = 'features';

  function getTotal() {
    $query = 'SELECT COUNT(objectid) FROM features WHERE deleted=0 AND ref_class<>"Article"';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }

  function process($current, $total) {
    $query = "SELECT objectid, refid, ref_class, created_datetime, edit_datetime, heading, displayable, modified_timestamp, authorid, summary, section, author, related_url1, related_url2 FROM features WHERE deleted=0  AND ref_class<>\"Article\" LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      // Feature not linked to article.
//      if ($row->ref_class == '') {
        $row->feature = 1;
        $row->body = '';
        $node = $this->saveArticle($row, "Feature");
      //watchdog('dadaimc2drupal', t("node: %node -> feature: %oid"), array("%node"=>$node->nid, "%oid"=>$row->objectid), WATCHDOG_DEBUG);
        $this->add_media_to_field(&$node, $row->objectid, $row->embedded_media);
        dadaimcpath_insert($node->nid, $row->objectid, NULL, 'feature');
 /*     } else {
        $aquery = "SELECT * from articles where objectid = $row->ref_id";
        $ares = $this->dbQuery($aquery);
        while ($arow = mysql_fetch_object($ares)) {
          $arow->feature = 1;
          $node = $this->saveArticle($arow, "Feature");
          $this->add_media_to_field(&$node, $arow->objectid, $arow->embedded_media);
          dadaimcpath_insert($node->nid, $row->objectid, NULL, 'feature');
        }
      }*/
      $this->updateContext();
      }
    }
}
