<?php
// $Id: categories.batch.inc,v 1.1.2.2 2010/11/15 00:09:19 jonhattan Exp $

require_once 'base.batch.inc';

/**
 * Batch operations. Import dadaIMC categories as taxonomy terms.
 *
 * Fields not imported:
 * created_datetime    | modified_timestamp  | modified_by | deleted | shortname | parentid | marked |
 */
class dadaImportCategories extends BaseBatchOperation implements iBatchOperation {
  public $token = 'categories';

  function getTotal() {
    $query = 'SELECT count(*) FROM categories WHERE deleted = 0';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }

  function process($current, $total) {
    // Preload vocabularies.
    $vocabularies = array();
    $query = "SELECT DISTINCT category_group FROM modules WHERE category_group != ''";
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $vocabularies[$row->category_group] = variable_get('dadaimc2drupal_vocabulary_'.$row->category_group, NULL);
    }
    // Import categories.
    $query = "SELECT objectid, catname, description, module FROM categories WHERE deleted = 0 LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $term = array(
        'vid' => $vocabularies[$row->module],
        'name' => $row->catname,
        'description' => $row->description
      );
      taxonomy_save_term($term);

      // Store the mapping.
      $record = array('objectid' => $row->objectid, 'tid' => $term['tid']);
      drupal_write_record('dada_categories', $record);

      $this->updateContext();
    }
  }
}
