<?php
// $Id: site.batch.inc,v 1.1.2.2 2010/12/13 21:34:24 jonhattan Exp $

require_once 'base.batch.inc';

/**
 * Example batch operation.
 */
class dadaImportSite extends BaseBatchOperation implements iBatchOperation {
  public $token = 'site';

  function getTotal() {
    return 1;
  }
  
  function process($current, $total) {
    // Sections.
    $sections = array();
    $vocab = array(
      'nodes' => array('story' => 1),
      'name' => 'Sections',
      'relations' => 0,
      'hierarchy' => 0,
      'multiple' => 0,
      'required' => 1,
      'tags' => 0,
    );
    taxonomy_save_vocabulary($vocab);
    $query = 'SELECT DISTINCT section FROM {articles}';
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $term = array(
        'vid' => $vocab['vid'],
        'name' => $row->section,
      );
      taxonomy_save_term($term);
      $sections[$row->section] = $term['tid'];
    }
    variable_set('dadaimc2drupal_sections_terms', $sections);

    $this->updateContext();
  }
}
